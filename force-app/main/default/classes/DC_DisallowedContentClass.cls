/*
 * Usage		:	Server side RESTful webservice class to parse and send back the banned URLs
 * Description	:	url mapping needs to be exposed in the client side callout
 */ 
@RestResource(urlMapping='/mycontent/*')
global with sharing class DC_DisallowedContentClass {
    /*
     * Server side method to identify disallowed urls from the client and send back.
     */
    @HttpGet
    global static String parseFileContent() {
        String bannedList = '';
        try{ 
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            String contentStr = req.requestURI.substring(req.requestURI.lastIndexOf('mycontent/')+10); 
            contentStr = contentStr.Substring(0,contentStr.length()-1); 
            
            //Custom Metadata Types to store the disallowed list of urls
            List<DC_BannedURL__mdt> disAllowedList = [Select DeveloperName, URLPath__c 
                                                      from DC_BannedURL__mdt 
                                                      Where Active__c = TRUE];
            Map<String, String> urlMap = new Map<String, String>();
            if(!disAllowedList.isEmpty()){
                for(DC_BannedURL__mdt b : disAllowedList)
                    urlMap.put(String.valueOf(b.URLPath__c), String.valueOf(b.URLPath__c)); 
                for(String s : contentStr.split(';')){ 
                    if(urlMap.containsKey(s))
                        bannedList += urlMap.get(s)+';';     
                }
            }
            bannedList = bannedList.endsWith(';') ? bannedList.removeEnd(';') : bannedList; 
            
        }Catch(exception e){
            System.debug(e.getMessage());    
        }
        return bannedList;
    } 
}