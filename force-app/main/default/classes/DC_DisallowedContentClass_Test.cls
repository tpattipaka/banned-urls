@isTest
public class DC_DisallowedContentClass_Test {
    @isTest
    static void bannedURLsTest(){ 
        //Test urls
        String bannedList = 'https://test1.com;https://test2.com;';
        String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
 		
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl+'/services/apexrest/'+bannedList;
        request.httpMethod = 'GET'; 
        RestContext.request = request;
        Test.startTest();
        String disAllowedURLs = DC_DisallowedContentClass.parseFileContent();
        System.assertNotEquals(disAllowedURLs, bannedList);
        Test.stopTest();
    }
}