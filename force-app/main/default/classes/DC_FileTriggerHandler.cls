/*
 * Trigger handler class invoked by the trigger on attachment object
 */ 
public class DC_FileTriggerHandler {
    /*
     * Parameter		Description				  	 					Type 		Required
     * fileList			attachments from the after insert trigger    	List		True
     */
    public static void processCaseFiles(List<Attachment> fileList){ 
        String urlStr = '';//semicolon (;) separated urls
        Map<String, String> fileMap = new Map<String, String>(); 
        for(Attachment att : fileList){  
            if(att.parentId.getSObjectType().getDescribe().getName() == 'Case'){ 
                //Correcting url formats
                String fileBody = DC_FileTriggerHandler.replaceFormat(att.body.toString()); 
                // Regular Expression to extract URL from the string
                String regex = '\\b((?:https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:, .;]*[-a-zA-Z0-9+&@#/%=~_|])'; 
                // Compile the Regular Expression
                Pattern p = Pattern.compile(regex);
                
                // Find the match between string and the regular expression
                Matcher m = p.matcher(fileBody);
                
                // Find the next subsequence of the input subsequence that find the pattern
                while (m.find()) { 
                    // Find the substring from the first index of match result
                    // to the last index of match  result and add in the list 
                    // Also encode/decode the Unicode characters in the weblinks
                    String urlFound = fileBody.substring(m.start(0), m.end(0)).unescapeHtml4();
                    urlStr += urlFound + ';';
                    fileMap.put(urlFound, att.Id);
                } 
            }
        } 
        urlStr = urlStr.endsWith(';') ? urlStr.removeEnd(';') : urlStr; 
        //asynchronous callout to server to identify and delete the banned url files
        DC_FileTriggerHandler.findBannedURLs(urlStr, fileMap); 
    } 
    
    /*
     * Parameter		Description				  	 					Type 		Required
     * urlPaths			urls found in the file content/body		    	String		True
     * fileMap			empty map to be filled based on the response	Map			True
     */
    @future(callout = true)
    public static void findBannedURLs(String urlPaths, Map<String, String> fileMap){
        try{ 
            //Custom settings to store the api keys
            DC_SFApiKeyManagement__c  ts = [Select Name, ApiKey__c, ApiSecret__c, ApiUrl__c, Username__c, Password__c, SecurityToken__c
                                            from DC_SFApiKeyManagement__c  Where Name__c = 'Sf_Oauth' LIMIT 1];	   
            
            String clientId = ts.ApiKey__c;
            String clientSecret = ts.ApiSecret__c;
            String username = ts.Username__c;
            String password = ts.Password__c + ts.SecurityToken__c;
            String urlPath = ts.ApiUrl__c; 
            
            String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
            //Access Code Generation for Oauth 2.0 autherization
            Http h = new Http(); 
            HttpRequest req = DC_FileTriggerHandler.makeRequest(reqbody, 'POST', urlPath+'/services/oauth2/token'); 
            HttpResponse res = h.send(req); 
            OAuth2 objAuth = (OAuth2)JSON.deserialize(res.getbody(), OAuth2.class); 
            String token = Test.isRunningTest() ? 'xyz' : objAuth.access_token;
            if(token != null){ 
                Http http = new Http();
                HttpRequest request = DC_FileTriggerHandler.makeRequest(null, 'GET', urlPath+'/services/apexrest/mycontent/'+urlPaths);
                request.setHeader('Authorization','Bearer '+token);
                HttpResponse response = http.send(request); 
                //delete the disallowed containing files/attachments from the client system
                DC_FileTriggerHandler.deleteDisallowedFiles((String)JSON.deserializeUntyped(response.getBody()), fileMap);
            }
        }Catch(exception e){
            System.debug(e.getMessage());    
        }
    }
    
    /*
     * Parameter		Description				  	 					Type 		Required
     * body				body to form a http request		    			String		True
     * method			type of method GET, POST, PATCH etc				String		True
     * endPoint			external system/server path uri					String		True
    */
    private static HttpRequest makeRequest(String body, String method, String endPoint){ 
        HttpRequest req = new HttpRequest();
        if(method != 'GET')
            req.setBody(body);
        else{
            req.setHeader('Content-Type','application/json');
            req.setHeader('accept','application/json'); 
        }
        req.setMethod(method);
        req.setEndpoint(endPoint);
        return req;
    }  
    
    /*
     * Parameter		Description				  	 					Type 		Required
     * originalTxt		original urls found in the file content		    String		True 
     */
    private static String replaceFormat(String originalTxt){
        originalTxt = originalTxt.replace('http://', 'https://');
        originalTxt = originalTxt.replace('www', 'https://www');
        originalTxt = originalTxt.replace('https://https://www', 'https://www');
        originalTxt = originalTxt.replace('http://https://www', 'https://www'); 
        return originalTxt;
    }
    
    /*
     * Parameter				Description				  	 					Type 		Required
     * disAllowedStr			Banned urls parsed from the server 			    String		True 
     * fileMap					map to store the file id, disallowed url		Map 		True
     */ 
    public static void deleteDisallowedFiles(String disAllowedStr, Map<String, String> fileMap){  
        try{ 
            List<String> disAllowedList = disAllowedStr.split(';');
            Set<Id> attIdSet = new Set<Id>();
            if(!disAllowedList.isEmpty()){
                for(String ds : disAllowedList){ 
                    if(fileMap.containsKey(ds))
                        attIdSet.add(fileMap.get(ds));  
                }
                List<Attachment> delAttList = [Select id, Name, ParentId from Attachment Where Id IN :attIdSet]; 
                if(!delAttList.isEmpty()){ 
                    Set<Id> caseIdsSet = new Set<Id>();
                    for(Attachment a : delAttList)
                        caseIdsSet.add(a.ParentId);
                    //Sending an email to notify the case owner on to be deleting files list.
                    DC_FileTriggerHandler.notifyCaseOwnerWithEmail(caseIdsSet);
                    delete delAttList;
                }    
            } 
        }Catch(exception e){
            System.debug(e.getMessage());    
        }
    }
    
    /*
     * Parameter				Description				  	 					Type 		Required
     * caseIds					Case ids to send find the owner details 		Set			True  
     */ 
    private static void notifyCaseOwnerWithEmail(Set<Id> caseIds){
        try{
            List<Case> caseList = [Select Id, CaseNumber, owner.Email, owner.Name from Case Where Id IN : caseIds];
            if(!caseList.isEmpty()){
                List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                for(Case c : caseList){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List<String> sendTo = new List<String>();
                    sendTo.add(c.owner.Email);
                    mail.setToAddresses(sendTo); 
                    mail.setReplyTo('noreply@salesforce.com'); 
                    mail.setSubject('Case Attachment Deletion Notification - ' + c.CaseNumber);
                    mail.setBccSender(false);
                    mail.setUseSignature(false);
                    String body = 'Hi ' + c.owner.Name + '</b>, <br /><br />';
                    body += 'The mentioned case found banned urls in the attachment section. Hence those disallowed files have been deleted. <br /><br />';
                    
                    body += '<br /><br />'+'Regards,<br />';
                    body += 'Salesforce Support.';
                    
                    mail.setHtmlBody(body);
                    mailList.add(mail);
                } 
                Messaging.sendEmail(mailList);
            }
        }Catch(exception e){
            System.debug(e.getMessage());    
        }   
    }
    
    /*To get aouthentication detail Wrapper*/
    public class OAuth2{
        public String id{get;set;}
        public String issued_at{get;set;}
        public String instance_url{get;set;}
        public String signature{get;set;}
        public String access_token{get;set;}    
    }
}